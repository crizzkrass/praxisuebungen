﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ATX.ViewModel;

namespace ATX.View
{
    /// <summary>
    /// Interaction logic for ATXView.xaml
    /// </summary>
    public partial class ATXView : Window
    {
        public ATXView()
        {
            InitializeComponent();
        }

        private void Filter(object sender, RoutedEventArgs e)
        {
            var vm = (ATXVM) DataContext;
            vm.FilterCommand.Execute(vm);
        }

    }
}
