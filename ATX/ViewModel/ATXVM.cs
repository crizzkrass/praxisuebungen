﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using ATX.Model;


namespace ATX.ViewModel
{
    class ATXVM : INotifyPropertyChanged
    {
        #region Properties

        private ATXModel atx = new ATXModel();
        public ATXModel Atx
        {
            get => atx;
            set
            {
                atx = value;
                RaisePropertyChanged();
            }
        }

        private DateTime date = DateTime.MinValue.Date;
        public DateTime Date
        {
            get => date;
            set
            {
                date = value;
                RaisePropertyChanged();
            }
        }

        private string vergleichDate = "==";
        public string VergleichDate
        {
            get => vergleichDate;
            set
            {
                vergleichDate = value;
                RaisePropertyChanged();
            }
        }

        private bool filterByDate = false;
        public bool FilterByDate
        {
            get => filterByDate;
            set
            {
                filterByDate = value;
                RaisePropertyChanged();
            }
        }

        private string preis = "0";
        public string Preis
        {
            get => preis;
            set
            {
                preis = value;
                RaisePropertyChanged();
            }
        }
        private string vergleichPreis = "==";
        public string VergleichPreis
        {
            get => vergleichPreis;
            set
            {
                vergleichPreis = value;
                RaisePropertyChanged();
            }
        }
        private bool filterByPreis = false;
        public bool FilterByPreis
        {
            get => filterByPreis;
            set
            {
                filterByPreis = value;
                RaisePropertyChanged();
            }
        }

        #endregion Properties

        #region Commands

        public ICommand FilterCommand { get; set; }

        #endregion

        public ATXVM()
        {
            
            FilterCommand = new RelayCommand(e =>
            {
                Atx.FilteredAndSortedStonks = new ObservableCollection<ATXKurs>(Atx.Stonks);

                if (FilterByDate)
                    Atx.FilterByDate(Date,
                            VergleichDate.Substring(VergleichDate.Length - 2));


                if (FilterByPreis)
                    Atx.FilterByPreis(
                        double.Parse(Preis, System.Globalization.CultureInfo.InvariantCulture),
                        VergleichPreis.Substring(VergleichDate.Length - 2));
                

            }, c => true);
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
