﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Animation;
using EFUebung.Context;
using EFUebung.Model;

namespace EFUebung.ViewModel
{
    class InvoiceVM
    {
        private Invoice toAdd = new Invoice();

        public Invoice ToAdd
        {
            get => toAdd;
            set
            {
                toAdd = value;
                RaisePropertyChanged();
            }
        }

        private Invoice toDelete = new Invoice();

        public Invoice ToDelete
        {
            get => toDelete;
            set
            {
                toDelete = value;
                RaisePropertyChanged();
            }
        }

        public IList<Invoice> InvoiceList
        {
            get
            {
                using (var ctx = new InvoiceContext())
                {
                    return ctx.Rechnungen.OrderByDescending(i => i.Id).ToList();
                }
            }
        }

        

        

        #region Commands

        public ICommand AddInvoiceCommand { get; private set; }
        public ICommand DeleteInvoiceCommand { get; private set; }

        #endregion



        public InvoiceVM()
        {
            AddInvoiceCommand = new RelayCommand(e =>
            {
                try{
                    using (var ctx = new InvoiceContext())
                    {
                        ctx.Rechnungen.Add(new Invoice
                        {
                            CustomerAddress = ToAdd.CustomerAddress,
                            CustomerName = ToAdd.CustomerName,
                            Amount = ToAdd.Amount,
                            InvoiceDate = ToAdd.InvoiceDate,
                            Vat = ToAdd.Vat
                        });
                        ctx.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                RaisePropertyChanged(nameof(InvoiceList));
            }, c => true);

            DeleteInvoiceCommand = new RelayCommand(e =>
            {
                try
                {
                    using (var ctx = new InvoiceContext())
                    {
                        var invoice = ctx.Rechnungen.Find(ToDelete.Id);
                        if (invoice != null)
                        {
                            ctx.Rechnungen.Remove(invoice);
                            ctx.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                RaisePropertyChanged(nameof(InvoiceList));
            }, c => true);
        }

        

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
