﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFUebung.Model;

namespace EFUebung.Context
{
    class InvoiceContext : DbContext
    {
        public DbSet<Invoice> Rechnungen { get; set; }

        public InvoiceContext()
        {
            Database.SetInitializer(new InvoiceInitialize());
        }
    }
}
