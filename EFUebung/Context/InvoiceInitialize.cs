﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFUebung.Model;

namespace EFUebung.Context
{
    class InvoiceInitialize : DropCreateDatabaseIfModelChanges<InvoiceContext>
    {
        protected override void Seed(InvoiceContext context)
        {
            IList<Invoice> defaults = new List<Invoice>();

            defaults.Add(new Invoice
            {
                CustomerName = "Customer1",
                Amount = 100,
                CustomerAddress = "Address1",
                InvoiceDate = new DateTime(2020, 01, 25),
                Vat = 10
            });

            defaults.Add(new Invoice
            {
                CustomerName = "Customer2",
                Amount = 200,
                CustomerAddress = "Address2",
                InvoiceDate = new DateTime(2020, 01, 15),
                Vat = 20
            });

            defaults.Add(new Invoice
            {
                CustomerName = "Customer3",
                Amount = 300,
                CustomerAddress = "Address3",
                InvoiceDate = new DateTime(2020, 01, 15),
                Vat = 30
            });

            defaults.Add(new Invoice
            {
                CustomerName = "Customer4",
                Amount = 400,
                CustomerAddress = "Address4",
                InvoiceDate = new DateTime(2020, 01, 15),
                Vat = 40
            });

            defaults.Add(new Invoice
            {
                CustomerName = "Customer5",
                Amount = 500,
                CustomerAddress = "Address5",
                InvoiceDate = new DateTime(2020, 01, 15),
                Vat = 50
            });

            base.Seed(context);
        }
    }
}
