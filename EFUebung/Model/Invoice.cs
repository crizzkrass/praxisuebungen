﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EFUebung.Model
{
    class Invoice
    {
        private int id;

        public int Id
        {
            get => id;
            set
            {
                id = value;
                RaisePropertyChanged();
            }
        }

        private String customerName;

        public String CustomerName
        {
            get => customerName;
            set
            {
                customerName = value;
                RaisePropertyChanged();
            }
        }

        private String customerAddress;

        public String CustomerAddress
        {
            get => customerAddress;
            set
            {
                customerAddress = value;
                RaisePropertyChanged();
            }
        }

        private double amount;

        public double Amount
        {
            get => amount;
            set
            {
                amount = value;
                RaisePropertyChanged();
            }
        }

        private DateTime invoiceDate;
        public DateTime InvoiceDate
        {
            get => invoiceDate;
            set
            {
                invoiceDate = value;
                RaisePropertyChanged();
            }
        }

        private int vat;

        public int Vat
        {
            get => vat;
            set
            {
                vat = value;
                RaisePropertyChanged();
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
