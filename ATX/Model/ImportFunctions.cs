﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATX.Model
{
    class ImportFunctions
    {
        #region Methods

        public static List<ATXKurs> ReadCSV(string filename)
        {
            string sCurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string sFile = System.IO.Path.Combine(sCurrentDirectory, @"..\..\..\",filename);
            string sFilePath = Path.GetFullPath(sFile);

            FileStream fs = File.Open(sFilePath, FileMode.Open, FileAccess.Read);
            BufferedStream bs = new BufferedStream(fs);
            StreamReader sr = new StreamReader(bs);
            List<string> lines = new List<string>();
            string s = string.Empty;
            while ((s = sr.ReadLine()) != null)
            {
                lines.Add(s);
            }

            var csv = from l in lines.Skip(1)
                let x = l.Replace("\"","").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => s)
                select new ATXKurs
                {
                    Date = DateTime.Parse(x.ElementAt(0), CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault),
                    Open = double.Parse(x.ElementAt(1), System.Globalization.CultureInfo.InvariantCulture),
                    High = double.Parse(x.ElementAt(2), System.Globalization.CultureInfo.InvariantCulture),
                    Low = double.Parse(x.ElementAt(3), System.Globalization.CultureInfo.InvariantCulture),
                    LastClose = double.Parse(x.ElementAt(4), System.Globalization.CultureInfo.InvariantCulture)
                };
            List<ATXKurs> import = csv.ToList();

            return import;
        }

        #endregion Methods

    }
}
