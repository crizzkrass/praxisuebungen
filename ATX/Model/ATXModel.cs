﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace ATX.Model
{
    class ATXModel
    {
        #region Properties
        
        public List<ATXKurs> Stonks { get; set; } = ImportFunctions.ReadCSV("Files\\ATX.csv");

        private ObservableCollection<ATXKurs> filteredAndSortedStonks;

        public ATXModel()
        {
            filteredAndSortedStonks = new ObservableCollection<ATXKurs>(this.Stonks);
        }

        public ObservableCollection<ATXKurs> FilteredAndSortedStonks
        {
            get => filteredAndSortedStonks;
            set
            {
                filteredAndSortedStonks.Clear();
                foreach (var x in value) filteredAndSortedStonks.Add(x);
            }
        }

        #endregion

        public void FilterByDate(DateTime date, string vergleich)
        {
            FilteredAndSortedStonks = new ObservableCollection<ATXKurs>(FilteredAndSortedStonks.Where(s =>
            {
                if (vergleich == "> ")
                    return DateTime.Compare(s.Date, date) > 0;

                if (vergleich == "< ")
                    return DateTime.Compare(s.Date, date) < 0;

                if (vergleich == "<=")
                    return DateTime.Compare(s.Date, date) <= 0;

                if (vergleich == ">=")
                    return DateTime.Compare(s.Date, date) >= 0;

                if (vergleich == "==")
                    return DateTime.Compare(s.Date, date) == 0;
                return false;
            }));
        }

        public void FilterByPreis(double preis, string vergleich)
        {
            FilteredAndSortedStonks = new ObservableCollection<ATXKurs>(FilteredAndSortedStonks.Where(s =>
            {
                bool a = false;
                if (vergleich == "> ")
                    a = s.Open > preis;

                if (vergleich == "< ")
                    a = s.Open < preis;

                if (vergleich == "<=")
                    a = s.Open <= preis;

                if (vergleich == ">=")
                    a = s.Open >= preis;

                if (vergleich == "==")
                    a = s.Open == preis;
                return a;
            }));


        }

        

    }
}
