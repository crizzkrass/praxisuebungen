﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ATX.Model
{
    class ATXKurs : INotifyPropertyChanged
    {
        #region Properties

        private DateTime date;

        public DateTime Date
        {
            get => date;
            set
            {
                date = value;
                RaisePropertyChanged();
            }
        }

        private double open;

        public double Open
        {
            get => open;
            set
            {
                open = value;
                RaisePropertyChanged();
            }
        }
        private double high;
        public double High
        {
            get => high;
            set
            {
                high = value;
                RaisePropertyChanged();
            }
        }
        private double low;
        public double Low
        {
            get => low;
            set
            {
                low = value;
                RaisePropertyChanged();
            }
        }
        private double lastClose;
        public double LastClose
        {
            get => lastClose;
            set
            {
                lastClose = value;
                RaisePropertyChanged();
            }
        }
        #endregion Properties

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
